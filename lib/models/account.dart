import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

@immutable
class Account{
  final String id;
  final String email;
  final String password;
  final String fullName;
  final String firstName;
  final String phone;
  final bool login;

  Account({
    this.id,
    this.email,
    this.password,
    this.fullName,
    this.firstName,
    this.phone,
    this.login
  });

  factory Account.fromMap(Map data) {
    return Account(
      id: data["id"],
      email: data["email"],
      password: data["password"],
      fullName: data["firstName"] + " " + data["lastName"],
      firstName: data["firstName"],
      phone: data["phone"],
      login: true
    );
  }
}