class Category {
  final int id;
  final String name;
  final String icon;
  Category(this.id, this.name, this.icon);
  factory Category.fromMap(Map map) {
    return Category(map["id"], map["name"], map["icon"]);
  }
}
