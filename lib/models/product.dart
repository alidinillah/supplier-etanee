
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Product{
  final String id;
  final String name;
  final String description;
  final String thumbnail;
  final int individualPrice;
  final double weight;

  Product(
    this.id,
    this.name,
    this.description,
    this.thumbnail,
    this.individualPrice,
    this.weight
  );

  factory Product.fromMap(Map map){
    return Product(map["id"], map["name"], map["description"], map["thumbnail"], map["individualPrice"], map["weight"]);
  }

  Widget imageSize(double size) {
    String imagePath = "assets/placeholder_product.png";
    Image image = Image.asset(
      imagePath,
      width: size,
      height: size,
    );
    return thumbnail == null
        ? image
        : CachedNetworkImage(
            imageUrl: thumbnail,
            placeholder: (_, __) => image,
            width: size,
            height: size,
          );
  }

  Widget get image {
    String imagePath = "assets/images/placeholder_product.png";
    Image image = Image.asset(imagePath);
    return thumbnail == null
        ? image
        : CachedNetworkImage(
            imageUrl: thumbnail,
            placeholder: (_, __) => image,
          );
  }
}