import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Util{
  static List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }
  
  static SnackBar getSnackBarError(String text) {
    final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text(
          text,
          style: TextStyle(color: Colors.white),
        ));
    return snackBar;
  }

  static String format(DateTime dt) {
    if (dt == null) return "-";
    var formatter = new DateFormat('dd MMMM yyyy');
    return formatter.format(dt);
  }

  static void showError(BuildContext context, String message, Widget icon) {
    Image errorImage = Image.asset(
      "assets/images/empty.png",
      width: 140,
    );
    icon = icon ?? errorImage;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Container(
            height: 200,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(12, 10, 12, 12),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Icon(
                          Icons.close,
                          color: Colors.grey,
                        ),
                        onTap: () => Navigator.pop(context),
                      ),
                    ],
                  ),
                  icon,
                  SizedBox(height: 10),
                  Text(
                    message,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  static void showConfirm(BuildContext context, String title, String message, String confirm, String cancel) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0)
          ),
          child: Container(
            height: 231,
            width: 328,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(height: 10.0),
                  Column(
                    children: <Widget>[
                      Text(title, style: TextStyle(fontSize: 16, color: Color(0xFF4F4F4F))),
                      SizedBox(height: 16.0),
                      Text(message, style: TextStyle(fontSize: 10, color: Color(0xFF4F4F4F))),
                    ],
                  ),
                  SizedBox(height: 32.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0)                      
                        ),
                        color: Color(0xFF68C93E),
                        onPressed: () {}, 
                        child: Text(
                          confirm,
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      FlatButton(
                        color: Colors.white,
                        onPressed: () {
                          Navigator.pop(context);
                        }, 
                        child: Text(
                          cancel,
                          style: TextStyle(color: Colors.red, fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
      );
      }
    );
  }
}