import 'package:dio/dio.dart';
import 'package:supplier/config/dio_config.dart';
import 'package:supplier/models/product.dart';

class ProductService {
  Future<List<Product>> products() async {
    Response response = await DioConfig.get().get("/product-public");
    List maps = response.data["content"];
    List<Product> products = maps.map((p) => Product.fromMap(p)).toList();
    return products;
  }
}