import 'package:dio/dio.dart';
import 'package:supplier/config/dio_config.dart';
import 'package:supplier/models/category.dart';

class CategoryService {
  Future<List<Category>> get() async {
    Response response =
        await DioConfig.get().get("/product-category-exclusive");
    List maps = response.data;
    List<Category> categories = maps.map((c) => Category.fromMap(c)).toList();
    return categories;
  }
}