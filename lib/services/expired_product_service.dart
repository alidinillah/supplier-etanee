// Halo
//

import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:supplier/config/dio_config.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';

class ExpiredProductService {
  Future<void> createExpiredProducts(
      List<ExpiredProduct> expiredProducts) async {
    List x = expiredProducts.map((ep) {
      Map o = {
        "productId": ep.product.id,
        "expireds": ep.expiredQuantities.map((eq) {
          Map e = {
            "date": DateFormat('yyyy-MMt-dd').format(eq.expired),
            "quantity": eq.quantity
          };
          return e;
        }).toList()
      };
      return o;
    }).toList();
    Response response =
        await DioConfig.get().post("/product-sn/batch", data: x);
    print(response);
  }

  // Future<List<ExpiredProduct>> getExpiredProducts() async {
  //   Response response = await DioConfig.get().get("/product-sn?sort=createDate,desc");
  //   // Response response = await DioConfig.get().get("/product-sn?page=0&size=5&sort=createDate,desc");
  //   print(response);
  //   List maps = await response.data["content"];
  //   List<ExpiredProduct> expiredProducts = maps.map((v) => ExpiredProduct.fromMap(v)).toList();

  //   return expiredProducts;
  // }

  Future<List<GetExpiredProduct>> getExpiredProducts() async {
    try {
      Response response = await DioConfig.get().get("/product-sn?page=0&size=50&sort=createDate,desc");
      List maps = await response.data["content"];
      List<GetExpiredProduct> expiredProducts = maps.map((v) => GetExpiredProduct.fromMap(v)).toList();
      print("expiredProducts:");
      // print(expiredProducts);
      return expiredProducts;
    } catch(e) {
      print(e);
      return null;
    }
  }

  Future<List<GetExpiredProduct>> getBySearch(String search) async {
    try {
      Response response = await DioConfig.get().get("/product-sn?search=code:$search&name:$search&sort=createDate,desc");
      List maps = await response.data["content"];
      List<GetExpiredProduct> expiredProducts = maps.map((v) => GetExpiredProduct.fromMap(v)).toList();
      // print("expiredProducts:");
      // print(expiredProducts);
      return expiredProducts;
    } catch(e) {
      print(e);
      return null;
    }
  }

  // Future<List<GetContent>> getContents() async {
  //   try{
  //     Response response =
  //         await DioConfig.get().get("/product-sn?sort=createDate,desc");
  //     List maps = response.data["content"];
  //     List<GetContent> contents = maps.map((v) => GetContent.fromMap(v)).toList();
  //     print("contents:"); 
  //     print(contents.toString());
  //     return contents;
  //   } catch(e){
  //     print(e);
  //     return null;
  //   }
  // }
}
