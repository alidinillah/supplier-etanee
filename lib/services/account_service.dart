import 'package:dio/dio.dart';
import 'package:supplier/config/dio_config.dart';
import 'package:supplier/models/account.dart';

class AccountService{
  Future<Account> login(String email, String password) async {
    Map param = {
      "email" : email.trim(),
      "password" : password
    };

    Response response = await DioConfig.get().post(
      "/login", 
      data: param);
    // print(response);
    Headers headers = response.headers;
    String token = headers.value("Token");
    DioConfig.setToken(token);
    return Account.fromMap(response.data["data"]);
  }

  Future<void> updateProfile(String fullname, String phone) async {
    List names = fullname.split(" ");
    var lastName = "";
    for(int i = 1; i < names.length; i++){
      lastName = lastName + names[i] + " ";
    }

    var param = {
      "firstName": names[0],
      "lastName": lastName.trim(),
      "phone": phone
    };

    Response response = await DioConfig.get().post(
      "/edit-profile",
      data: param,
    );
    return response.data["data"];
  }

  Future<void> updatePassword(String oldPassword, String newPassword) async {
    var param = {
      "oldPassword": oldPassword,
      "newPassword": newPassword,
    };

    Response response = await DioConfig.get().post(
      "/changePassword",
      data: param,
    );
    return response.data["data"];
  }
}