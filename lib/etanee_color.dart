import 'package:flutter/material.dart';

class EtaneeColor {
  static int main = 0xFF68c93e;
  static const Color c1 = Color(0xFF68c93e);
  static const Color c2 = Color(0xFF2c3c8d);
  static const Color c3 = Color(0xFFf4a622);
  static const Color c4 = Color(0xFF828282);
  static const Color c10 = Color(0xFFFFFFFF);
}
