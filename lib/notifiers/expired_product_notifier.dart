import 'package:flutter/cupertino.dart';
import 'package:supplier/models/product.dart';

class ExpiredProductNotifier extends ChangeNotifier {
  List<ExpiredProduct> expiredProducts;
  ExpiredProductNotifier() : expiredProducts = [];

  addProduct(Product product) {
    ExpiredProduct ep = ExpiredProduct(product, []);
    expiredProducts.add(ep);
    notifyListeners();
  }

  removeProduct(ExpiredProduct selectedEp) {
    expiredProducts.remove(selectedEp);
    notifyListeners();
  }

  cleanProduct() {
    expiredProducts = [];
    notifyListeners();
  }

  int get total => expiredProducts != null ? expiredProducts.length : 0;

  addExpired(ExpiredProduct selectedEp) {
    ExpiredQuantity eq = ExpiredQuantity(DateTime.now().millisecondsSinceEpoch, null, 0);
    selectedEp.expiredQuantities.add(eq);
    notifyListeners();
  }

  removeExpired(ExpiredQuantity expiredQuantity) {
    for (int i = 0; i < expiredProducts.length; i++) {
      ExpiredProduct e = expiredProducts[i];
        e.expiredQuantities.removeAt(i);
    }
    notifyListeners();
  }

  ExpiredProduct getExpiredProduct (Product product) {
    ExpiredProduct selectedEp;
    for (int i = 0; i < total; i++) {
      ExpiredProduct ep = expiredProducts[i];
      if(ep.product.id == product.id) {
        selectedEp = ep;
        break;
      }
    }
    return selectedEp;
  }

  setDate(Product product, int id, DateTime date) {
    ExpiredProduct selectedEp = getExpiredProduct(product);
    ExpiredQuantity selectedEq;
    for (int i = 0; i < selectedEp.expiredQuantities.length; i++) {
      ExpiredQuantity eq = selectedEp.expiredQuantities[i];
      if (eq.id == id) {
        selectedEq = eq;
        break;
      }
    }
    selectedEq.expired = date;
    notifyListeners();
  }

  setQuantity(Product product, int id, int quantity) {
    ExpiredProduct selectedEp = getExpiredProduct(product);
    ExpiredQuantity selectedEq;
    for (int i = 0; i < selectedEp.expiredQuantities.length; i++) {
      ExpiredQuantity eq = selectedEp.expiredQuantities[i];
      if (eq.id == id) {
        selectedEq = eq;
        break;
      }
    }
    selectedEq.quantity = quantity;
    notifyListeners();
  }

  isExist(Product product) {
    ExpiredProduct ep = getExpiredProduct(product);
    return ep == null;
  }

  int totalQuantity(Product product) {
    int total = 0;
    ExpiredProduct selectedEp = getExpiredProduct(product);
    for (int i = 0; i < selectedEp.expiredQuantities.length; i++) {
      ExpiredQuantity selectedEq = selectedEp.expiredQuantities[i];
      total += selectedEq.quantity;
    }
    return total;
  }
}

class ExpiredProduct {
  DateTime now = DateTime.now();
  Product product;
  List<ExpiredQuantity> expiredQuantities;
  ExpiredProduct(this.product, this.expiredQuantities); 

  
  factory ExpiredProduct.fromMap(Map<String, dynamic> map) {
    Product product = Product.fromMap(map["product"]);
    return ExpiredProduct(product,
      List<ExpiredQuantity>.from(
        map["expireds"].map((x) => ExpiredQuantity.fromMap(x))),
    );
  }
}

class ExpiredQuantity {
  int id;
  DateTime expired;
  int quantity;
  ExpiredQuantity(this.id, this.expired, this.quantity);

  factory ExpiredQuantity.fromMap(Map<String, dynamic> map) => 
    ExpiredQuantity(
      map["id"],
      DateTime.parse(map["expired"]),
      map["quantity"],
    );
}

class GetExpiredProduct {
  Product product;
  String code;
  DateTime expired;
  DateTime createDate;
  GetExpiredProduct(this.product, this.code, this.expired, this.createDate); 

  
  factory GetExpiredProduct.fromMap(Map<String, dynamic> map) {
    Product product = Product.fromMap(map["product"]);
    return GetExpiredProduct(product,
      map["id"],
      DateTime.parse(map["expired"]),
      DateTime.parse(map["createDate"])
    );
  }
}
