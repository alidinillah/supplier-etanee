import 'package:flutter/material.dart';
import 'package:supplier/models/account.dart';

enum AuthStatus {
  Authenticated,
  Unauthenticated
}

class AuthNotifier with ChangeNotifier {
  AuthStatus _status = AuthStatus.Unauthenticated;
  Account _account = Account(fullName: "Anonim", login: false);

  set status(AuthStatus s) {
    _status = s;
    notifyListeners();
  }

  get status => _status;

  set account(Account a) {
    _account = a;
    notifyListeners();
  }

  get account => _account;

  Future signOut() async {
    _status = AuthStatus.Unauthenticated;
    _account = Account(login: false);
    notifyListeners();
    return Future.delayed(Duration.zero);
  }
}
