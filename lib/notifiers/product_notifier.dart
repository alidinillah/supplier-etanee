import 'package:flutter/material.dart';

class ProductNotifier extends ChangeNotifier {
  String _search = "";
  get search => _search;

  set search(String s) {
    _search = s;
    notifyListeners();
  }

}