import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EmptyProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SvgPicture.asset('empty_state.svg'),
            SizedBox(height: 32),
            Text('Produk Tidak Ditemukan', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 14, fontWeight: FontWeight.bold)),
            SizedBox(height: 16),
            Text('Periksa kembali nama produk', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 14)),
          ],
        ),
      ),
    );
  }
}
