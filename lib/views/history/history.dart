import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/notifiers/search_notifier.dart';
import 'package:supplier/services/expired_product_service.dart';
import 'package:supplier/util.dart';
import 'package:supplier/views/empty/empty_product.dart';
import 'package:supplier/views/loading.dart';
import 'package:supplier/views/print/print_page.dart';

class History extends StatelessWidget {
  final String search;
  History(this.search);

  @override
  Widget build(BuildContext context) {
    String now = Util.format(DateTime.now());
    int quantity = 1;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SearchNotifier>(
          create: (_) => SearchNotifier(search)
        ),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: Text('Riwayat'),
          elevation: 1.5,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(75.0),
            child: Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
              child: Consumer<SearchNotifier>(
                builder: (_, sn, __) => TextFormField(
                  initialValue: sn.search,
                  onChanged: (s) => sn.search = s,
                  cursorColor: Color(0xFF68C93E),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(
                        Radius.circular(6.0),
                      ),
                    ),
                    filled: true,
                    fillColor: Color(0xFFF2F2F2),
                    focusColor: Colors.grey,
                    hintText: "Cari riwayat produk disini",
                    suffixIcon: IconButton(
                      icon: Icon(Icons.search, color: Color(0xFF4F4F4F), size: 30), 
                      onPressed: (){}
                    ),
                    contentPadding: EdgeInsets.all(16.0),
                  ),
                ),
              ),
            ),
          ),
        ),
        //       var k = '';
        // var n = '';
        // int quantity = 1;
        // for (int i = 0; i < test.length; i++) {
        //     if(test[i]["date"] != k || test[i]["name"] != n) {
              
        //       k = test[i]["date"];
        //       n = test[i]["name"];
        //       print(k);
        //       print(n);
        //     } else {
              
        //     }
        // }
        backgroundColor: Color(0xFFF2F2F2),
        body: FutureBuilder<List<GetExpiredProduct>>(
          future: ExpiredProductService().getExpiredProducts(),
          builder: (_, snapshot) {
            if(snapshot.data == null) {
              return Loading();
            } else {
              return Consumer<SearchNotifier> (
                builder: (_, sn, __) {
                  List<GetExpiredProduct> expProducts = snapshot.data.where((e) {
                    bool s = Util.format(e.createDate)
                              .toString()
                              .toLowerCase()
                              .contains(sn.search.toLowerCase()) ||
                             e.product.name
                              .toLowerCase()
                              .contains(sn.search.toLowerCase());
                            return s;
                  }).toList();
                  if (expProducts.isEmpty) return EmptyProduct();
                  return ListView.builder(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemCount: expProducts.length,
                    itemBuilder: (_, i) {
                          return ListView.builder(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemCount: expProducts.length,
                            itemBuilder: (_, j) {
                              if(expProducts[j].createDate.toString() != now) {
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 35,
                                      color: Color(0xFFEFFFE9),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                                        child: Text(Util.format(expProducts[j].createDate), style: TextStyle(color: Color(0xFF4F4F4F)),),
                                      ),
                                    ),
                                    Padding(
                                              padding: const EdgeInsets.only(bottom: 0),
                                              child: Container(
                                                color: Colors.white,
                                                child: Padding(
                                                  padding: EdgeInsets.all(16),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Text(expProducts[j].product.name, style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                                                      SizedBox(height: 8),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: <Widget>[
                                                          Text('Jumlah : ', style: TextStyle(color: Color(0xFF4F4F4F))),
                                                          Text((quantity+=i).toString() + ' karton', style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold))
                                                        ],
                                                      ),
                                                      SizedBox(height: 8),
                                                      Center(
                                                        child: RaisedButton(
                                                          elevation: 0,
                                                          onPressed: (){
                                                            Navigator.push(context, MaterialPageRoute(
                                                              builder: (context) => PrintPage()));
                                                          },
                                                          color: Color(0xFFEDECFF),
                                                          child: Text('Cetak Ulang', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF9690DD))),
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(20)
                                                          ),
                                                        ),
                                                      ),
                                                      if(i == snapshot.data.length) 
                                                        Divider(color: Colors.grey)
                                                      else 
                                                        Container()
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            )
                                  ],
                                );
                              } else {
                                return Container();
                              }
                            }
                          );
                    }
                  );
                }
              );
            }
          }
        ),
      ),
    );
  }
}