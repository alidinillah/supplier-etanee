import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:supplier/config/dio_config.dart';
import 'package:supplier/models/account.dart';
import 'package:supplier/notifiers/auth_notifier.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/views/account/update_account.dart';
import 'package:supplier/views/account/update_password.dart';
import 'package:supplier/views/loading.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthNotifier notifier = Provider.of<AuthNotifier>(context, listen: false);
    Account account = notifier.account;
    double width = MediaQuery.of(context).size.width;
    Widget background = SvgPicture.asset('assets/account_background.svg',
      fit: BoxFit.fill,
      width: width
    );
    if (account == null) return Loading();
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                background,
                Padding(
                  padding: const EdgeInsets.only(top: 40.0, right: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        elevation: 0,
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => UpdateAccount(account)));
                        },
                        color: Color(0xFF99DE7B),
                        child: Text('Ubah Profil', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 100),
                      CircleAvatar(
                        radius: 38,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          radius: 35,
                          backgroundImage: AssetImage('assets/account.png')
                        ),
                      ),
                      SizedBox(height: 24),
                      Text(account.fullName, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),),
                      SizedBox(height: 8),
                      Text(account.email, style: TextStyle(color: Colors.white))
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('No. Telepon', style: TextStyle(color: Color(0xFF4F4F4F))),
                  Text(account.phone, style: TextStyle(color: Color(0xFFBDBDBD))),
                ],
              ),
            ),
            Divider(height: 30),
            Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Password', style: TextStyle(color: Color(0xFF4F4F4F))),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => UpdatePassword()));
                    },
                    child: Text('Ubah Password', style: TextStyle(color: Color(0xFF68C93E), fontWeight: FontWeight.bold))
                  ),
                ],
              ),
            ),
            Divider(height: 30),
            Center(
              child: GestureDetector(
                onTap: () {
                  Provider.of<ExpiredProductNotifier>(context).cleanProduct();
                  DioConfig.clearToken();
                  notifier.signOut();
                  // Navigator.push(context, MaterialPageRoute(builder: (_) => Login()));
                },
                child: Container(
                  width: 328,
                  height: 50,
                  child: Center(child: Text('Keluar', style: TextStyle(fontSize: 16, color: Colors.red, fontWeight: FontWeight.bold))),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}