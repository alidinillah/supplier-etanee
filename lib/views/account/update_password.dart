import 'package:flutter/material.dart';
import 'package:supplier/services/account_service.dart';
import 'package:supplier/util.dart';
import 'package:supplier/views/menu/bottom_menu.dart';

class UpdatePassword extends StatefulWidget {
  @override
  _UpdatePasswordState createState() => new _UpdatePasswordState();
}

class _UpdatePasswordState extends State<UpdatePassword> {
  bool oldPassword = true;
  bool newPassword = true;
  bool confirmPassword = true;
  TextEditingController _controllerOldPassword = TextEditingController();
  TextEditingController _controllerNewPassword = TextEditingController();
  TextEditingController _controllerConfirmPassword = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void togglePasswordVisibility(){
    setState(() {
      oldPassword = !oldPassword;
      newPassword = !newPassword;
      confirmPassword = !confirmPassword;
    });
  }

  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          elevation: 1.5,
          title: Text('Ubah Password'),
          // leading: Icon(Icons.arrow_back, color: Colors.black),
        ),
        body: ListView(
          shrinkWrap: true,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 500,
              padding: EdgeInsets.only(top:20.0),
              child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextFormField(
                    obscureText: oldPassword,
                    controller: _controllerOldPassword,
                    cursorColor: Color(0xFF68C93E),
                    initialValue: null,
                    decoration: InputDecoration(
                      labelText: 'Password Lama',
                      labelStyle:
                        TextStyle(color: Colors.grey, fontSize: 16),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFF68C93E))
                      ),
                      suffixIcon: GestureDetector(
                        onTap: (){
                          togglePasswordVisibility();
                        },
                      child: Icon(
                        oldPassword ? Icons.visibility_off : Icons.visibility,
                          color: oldPassword ? Colors.grey : Color(0xFF68C93E)
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 32.0),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextFormField(
                    controller: _controllerNewPassword,
                    obscureText: newPassword,
                    initialValue: null,
                    decoration: InputDecoration(
                      labelText: 'Password Baru',
                      labelStyle:
                        TextStyle(color: Colors.grey, fontSize: 16),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFF68C93E))
                      ),
                      suffixIcon: GestureDetector(
                        onTap: (){
                          togglePasswordVisibility();
                        },
                        child: Icon(
                          newPassword ? Icons.visibility_off : Icons.visibility,
                          color: newPassword ? Colors.grey : Color(0xFF68C93E),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 32.0),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextFormField(
                    controller: _controllerConfirmPassword,
                    obscureText: confirmPassword,
                    cursorColor: Color(0xFF68C93E),
                    initialValue: null,
                    decoration: InputDecoration(
                      labelText: 'Konfirmasi Password Baru',
                      labelStyle:
                        TextStyle(color: Colors.grey, fontSize: 16),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFF68C93E))
                        ),
                      suffixIcon: GestureDetector(
                        onTap: (){
                          togglePasswordVisibility();
                        },
                        child: Icon(
                          confirmPassword ? Icons.visibility_off : Icons.visibility,
                          color: confirmPassword ? Colors.grey : Color(0xFF68C93E),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ButtonTheme(
              minWidth: 328,
              height: 48,
              child: RaisedButton(
                elevation: 0,
                color: Color(0xFF68C93E),
                onPressed: () => _update(context),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)
                ),
                child: Text('Simpan', style: TextStyle(fontSize: 16, color: Colors.white)),
              ),
            ),
          )
        ),
    );
  }

  bool _validate() {
    if (_controllerOldPassword.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(Util.getSnackBarError("Password lama harus diisi"));
      return false;
    }
    if (_controllerNewPassword.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(Util.getSnackBarError("Password baru harus diisi"));
      return false;
    }
    if (_controllerConfirmPassword.text != _controllerNewPassword.text){
      _scaffoldKey.currentState
      .showSnackBar(Util.getSnackBarError("Konfirmasi password tidak sesuai"));
      return false;
    }
    return true;
  }

  void _update(context) async {
    
    if (!_validate()) return;
    await AccountService().updatePassword(_controllerOldPassword.text, _controllerNewPassword.text)
    .catchError((e) => print(e))
    .then((r){
      Util.getSnackBarError('Berhasil diubah');
      Navigator.push(context, MaterialPageRoute(builder: (_) => BottomMenu(2)));
    });
  }
}

