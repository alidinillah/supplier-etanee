import 'package:flutter/material.dart';
import 'package:supplier/models/account.dart';
import 'package:supplier/services/account_service.dart';
import 'package:supplier/util.dart';
import 'package:supplier/views/menu/bottom_menu.dart';

class UpdateAccount extends StatefulWidget {
  final Account account;
  UpdateAccount(this.account);

  @override
  _UpdateAccountState createState() => new _UpdateAccountState();
}

class _UpdateAccountState extends State<UpdateAccount> {
  bool _isFieldNameValid;
  bool _isFieldPhoneValid;
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    if (widget.account != null) {
      _controllerName.text = widget.account.fullName;
      _controllerPhone.text = widget.account.phone;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        elevation: 1.5,
        title: Text('Ubah Profil'),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
            child: TextFormField(
              controller: _controllerName,
              cursorColor: Color(0xFF68C93E),
              decoration: InputDecoration(
                labelText: 'Nama Pengguna',
                labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFF68C93E)
                  ),
                ),
              ),  
              onChanged: (value) {
                bool isFieldValid = value.trim().isNotEmpty;
                if (isFieldValid != _isFieldNameValid) {
                  setState(() => _isFieldNameValid = isFieldValid);
                }
              },
            ),
          ),
          SizedBox(height: 24),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: TextFormField(
              controller: _controllerPhone,
              keyboardType: TextInputType.number,
              cursorColor: Color(0xFF68C93E),
              decoration: InputDecoration(
                labelText: 'No. Telepon',
                labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFF68C93E)
                  ),
                ),
              ),
              onChanged: (value) {
                bool isFieldValid = value.trim().isNotEmpty;
                if (isFieldValid != _isFieldPhoneValid) {
                  setState(() => _isFieldPhoneValid = isFieldValid);
                }
              },
            ),
          ),
      ],
    ),
    bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            minWidth: 328,
            height: 48,
            child: RaisedButton(
              elevation: 0,
              color: Color(0xFF68C93E),
              onPressed: () => _update(context),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
              ),
              child: Text('Simpan', style: TextStyle(fontSize: 16, color: Colors.white)),
            ),
          ),
        )
      ),
    );
  }
  bool _validate() {
    if (_controllerName.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(Util.getSnackBarError("Nama pengguna harus diisi"));
      return false;
    }
    if (_controllerPhone.text.isEmpty) {
      _scaffoldKey.currentState
          .showSnackBar(Util.getSnackBarError("Nomor telepon harus diisi"));
      return false;
    }
    return true;
  }

  void _update(context) async {
    if (!_validate()) return;
    await AccountService().updateProfile(_controllerName.text, _controllerPhone.text)
    .catchError((e) => print(e))
    .then((r) {
      Navigator.push(context, MaterialPageRoute(builder: (_)=> BottomMenu(2)));
    });
  }
}
