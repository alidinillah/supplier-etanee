import 'package:flutter/material.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';

class CodeCheckItems extends StatelessWidget {
  final GetExpiredProduct getExpiredProduct;
  CodeCheckItems(this.getExpiredProduct);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Card(
        child: Text(getExpiredProduct.code),
      ),
    );
  }
}