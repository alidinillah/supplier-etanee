import 'package:blue_thermal_printer/blue_thermal_printer.dart';

class PrintItem {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;

   sample(String pathImage) async {
    //SIZE
    // 0- normal size text
    // 1- only bold text
    // 2- bold with medium text
    // 3- bold with large text
    //ALIGN
    // 0- ESC_ALIGN_LEFT
    // 1- ESC_ALIGN_CENTER
    // 2- ESC_ALIGN_RIGHT
    bluetooth.isConnected.then((isConnected) {
      if (isConnected) {
        bluetooth.printImage(pathImage); 
        bluetooth.printNewLine();
        bluetooth.printCustom("----------------------------------",0,1);
        bluetooth.printNewLine();
        bluetooth.printCustom("Ayam 0.7-0.8 Kg [1 Ekor - Potong 10 Bagian]",1,1);
        bluetooth.printNewLine();
        bluetooth.printCustom("Tanggal Kadaluarsa",0,1);
        bluetooth.printCustom("12 Juni 2020",1,1);
        bluetooth.printNewLine();
        bluetooth.printQRcode("VP82RETG", 300, 300, 1);
        bluetooth.printNewLine();
        bluetooth.printCustom("Nomor Serial",0,1);
        bluetooth.printCustom("VP82RETG",2,1);
        bluetooth.printNewLine();
        bluetooth.paperCut();
      }
    });
  }
}