import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Print extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            SvgPicture.asset('assets/printer.svg'),
            SizedBox(height: 48),
            Text('Mohon Tunggu', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold)),
            SizedBox(height: 8),
            Text('QR Code produk Anda sedang dalam proses cetak', style: TextStyle(color: Color(0XFF4F4F4F), height: 1.5), textAlign: TextAlign.center),
            // Image.asset('assets/loading.mp4')
          ]
        ),
      ),
    );
  }
}