import 'dart:io';
import 'dart:typed_data';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/views/print/print_done.dart';
import 'package:supplier/views/print/print_item.dart';

class PrintPage extends StatefulWidget {
  @override
  _PrintPageState createState() => new _PrintPageState();
}

class _PrintPageState extends State<PrintPage> {

  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;

  List<BluetoothDevice> _devices = [];
  BluetoothDevice _device;
  bool _connected = false;
  String pathImage;
  PrintItem printItem;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    initSavetoPath();
    printItem = PrintItem();
  }

  initSavetoPath()async{
    //read and write
    //image max 300px X 300px
    final filename = 'etaneelogo.png';
    var bytes = await rootBundle.load("assets/etaneelogo.png");
    String dir = (await getApplicationDocumentsDirectory()).path;
    writeToFile(bytes,'$dir/$filename');
    setState(() {
      pathImage='$dir/$filename';
    });
  }


  Future<void> initPlatformState() async {
    bool isConnected=await bluetooth.isConnected;
    List<BluetoothDevice> devices = [];
    try {
      devices = await bluetooth.getBondedDevices();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    bluetooth.onStateChanged().listen((state) {
      switch (state) {
        case BlueThermalPrinter.CONNECTED:
          setState(() {
            _connected = true;
          });
          break;
        case BlueThermalPrinter.DISCONNECTED:
          setState(() {
            _connected = false;
          });
          break;
        default:
          print(state);
          break;
      }
    });

    if (!mounted) return;
    setState(() {
      _devices = devices;
    });

    if(isConnected) {
      setState(() {
        _connected=true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Cetak QR Code'),
          elevation: 1.5,
        ),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Printer :',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Color(0XFF4F4F4F)
                          ),
                        ),
                        SizedBox(width: 16),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                          height: 44,
                          width: MediaQuery.of(context).size.width-100,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey
                            ),
                            borderRadius: BorderRadius.circular(4)
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              items: _getDeviceItems(),
                              onChanged: (value) => setState(() => _device = value),
                              value: _device,
                            ),
                          ),
                        ),
                      ],
                    ),
                        SizedBox(height: 16),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            SizedBox(width: 70),
                            Expanded(
                              child: FlatButton(
                                color: Color(0xFFEDECFF),
                                onPressed:() {
                                  initPlatformState();
                                },
                                child: Text('Refresh', style: TextStyle(color: Color(0xFF9690DD), fontSize: 12, fontWeight: FontWeight.bold)),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)
                                ),
                              ),
                            ),
                            SizedBox(width: 16),
                            Expanded(
                              child: FlatButton(
                                color: _connected ? Color(0xFF9690DD): Color(0xFFEDECFF),
                                onPressed:
                                _connected ? _disconnect : _connect,
                                child: Text(_connected ? 'Putuskan' : 'Sambungkan', style: TextStyle(color: _connected ? Color(0xFFEDECFF) : Color(0xFF9690DD), fontSize: 12, fontWeight: FontWeight.bold)),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)
                                ),
                              ),
                            ),
                          ],
                        ),
                  ],
                ),
                SizedBox(height: 171),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'Jika printer bluetooth Anda tidak ditemukan, silakan sambungkan melalui ',
                    style: TextStyle(fontSize: 14, color: Color(0XFF4F4F4F), height: 1.5, fontFamily: "poppins"),  
                    children: <TextSpan>[
                      TextSpan(text: 'Pengaturan Bluetooth', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF68C93E)),
                      recognizer: TapGestureRecognizer()..onTap = (){
                        AppSettings.openBluetoothSettings();
                        },
                      ),
                      TextSpan(
                        text: ' perangkat Anda',
                        style: TextStyle(fontSize: 14, color: Color(0XFF4F4F4F), height: 1.5)
                      )
                    ],
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 50),
                //   child:  RaisedButton(
                //     color: Colors.brown,
                //     onPressed:(){
                //       printItem.sample(pathImage);
                //     },
                //     child: Text('Cetak', style: TextStyle(color: Colors.white)),
                //   ),
                // ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            minWidth: 328,
            height: 48,
            child: RaisedButton(
              elevation: 0,
              color: Color(0xFF68C93E),
              onPressed: () {
                printItem.sample(pathImage);
                Provider.of<ExpiredProductNotifier>(context).cleanProduct();
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => PrintDone()));
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
              ),
              child: Text('Cetak', style: TextStyle(fontSize: 16, color: Colors.white)),
            ),
          ),
        )
      ),
    );
  }


  List<DropdownMenuItem<BluetoothDevice>> _getDeviceItems() {
    List<DropdownMenuItem<BluetoothDevice>> items = [];
    if (_devices.isEmpty) {
      items.add(DropdownMenuItem(
        child: Text('Pilih printer                                    ', 
              style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 14)),
      ));
    } else {
      _devices.forEach((device) {
        items.add(DropdownMenuItem(
          child: Text(device.name, style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 14)),
          value: device,
        ));
      });
    }
    return items;
  }


  void _connect() {
    if (_device == null) {
      show('Tidak ada printer yang dipilih.');
    } else {
      bluetooth.isConnected.then((isConnected) {
        if (!isConnected) {
          bluetooth.connect(_device).catchError((error) {
            print(error);
            setState(() => _connected = false);
          });
          setState(() => _connected = true);
        }
      });
    }
  }

  void _disconnect() {
    bluetooth.disconnect();
    setState(() => _connected = true);
  }

//write to app path
  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  Future show(
      String message, {
        Duration duration: const Duration(seconds: 3),
      }) async {
    await Future.delayed(Duration(milliseconds: 100));
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        duration: duration,
      ),
    );
  }
}