import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:supplier/views/menu/bottom_menu.dart';

class PrintDone extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            SvgPicture.asset('assets/check_circle.svg'),
            SizedBox(height: 48),
            Text('Selesai', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold)),
            SizedBox(height: 48),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: 'QR Code produk telah berhasil dicetak.\nAnda dapat melihat daftar kode produk terbaru di halaman ',
                style: TextStyle(fontSize: 14, color: Color(0XFF4F4F4F), height: 1.5, fontFamily: "poppins"),  
                children: <TextSpan>[
                  TextSpan(text: 'Riwayat', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF68C93E)),
                  recognizer: TapGestureRecognizer()..onTap = (){
                    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                      builder: (context) => BottomMenu(1)),
                      (Route<dynamic> route) => false);
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 8),
          ]
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            minWidth: 328,
            height: 48,
            child: RaisedButton(
              elevation: 0,
              color: Color(0xFF68C93E),
              onPressed: (){
                Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                      builder: (context) => BottomMenu(0)),
                      (Route<dynamic> route) => false);
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
              ),
              child: Text('Ke Halaman Utama', style: TextStyle(fontSize: 16, color: Colors.white)),
            ),
          ),
        )
      ),
    );
  }
}