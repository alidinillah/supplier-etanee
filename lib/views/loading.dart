import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(10),
        height: 50,
        width: 50,
        child: CircularProgressIndicator(
          // backgroundColor: EtaneeColor.c1,
          valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF68C93E)),
        ),
      ),
    );
  }
}