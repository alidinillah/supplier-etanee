import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:supplier/views/account/account_page.dart';
import 'package:supplier/views/history/history.dart';
import 'package:supplier/views/home/home.dart';

class BottomMenu extends StatefulWidget {
  final int page;
  BottomMenu(this.page);
  @override
  _BottomMenuState createState() => _BottomMenuState();
}

class _BottomMenuState extends State<BottomMenu> {

  int _currentIndex = 0;
  @override
  void initState() {
    super.initState();
    _currentIndex = widget.page;
  }
  final List<Widget> _pages = [
      Home(),
      History(null),
      AccountPage(),
  ];

  void onTapped(int index){
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTapped,
        currentIndex: _currentIndex,
        selectedItemColor: Color(0xFF68C93E),
        items: 
      [
        BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/home.svg'),
          activeIcon: SvgPicture.asset('assets/home.svg', color: Color(0xFF68C93E)),
          title: Text('Beranda'),
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/history.svg'),
          activeIcon: SvgPicture.asset('assets/history.svg', color: Color(0xFF68C93E)),
          title: Text('Riwayat'),
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/profile.svg'),
          activeIcon: SvgPicture.asset('assets/profile.svg', color: Color(0xFF68C93E)),
          title: Text('Akun')
        ),
      ]
      ),
    );
  }
}