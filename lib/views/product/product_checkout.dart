import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/views/product/product_item_checkout.dart';

class ProductCheckout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<ExpiredProductNotifier>(
        builder: (_, epn, __) {
          if (epn == null) return null;
          return ListView.separated(
            separatorBuilder: (_, __) => Padding(
              padding: EdgeInsets.only(bottom: 8.0)), 
            shrinkWrap: true,
            itemCount: epn.expiredProducts.length,
            itemBuilder: (ctx, i) => ProductItemCheckout(epn.expiredProducts[i]),
            physics: ScrollPhysics(),
          );
        }
      ),
    );
  }
}