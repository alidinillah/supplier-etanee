import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/util.dart';

class ProductItemCheckout extends StatefulWidget {
  final ExpiredProduct expiredProduct;
  ProductItemCheckout(this.expiredProduct);

  @override
  _ProductItemCheckoutState createState() => _ProductItemCheckoutState();
}

class _ProductItemCheckoutState extends State<ProductItemCheckout> {
  
  List<Widget> tanggalKadaluarsaWidgets(ExpiredProductNotifier epn) {
    return widget.expiredProduct.expiredQuantities.map((item) {
      TextEditingController _dateController = TextEditingController();
      TextEditingController _quantityController = TextEditingController();
      _dateController.text = item.expired == null ? '' : Util.format(item.expired);
      _quantityController.text = item.quantity.toString() ?? 0;
      DateTime now = DateTime.now();
      return Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  width: 190,
                  height: 44,
                  child: TextFormField(
                    style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F)),
                    controller: _dateController,
                    keyboardType: null,
                    showCursor: false,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(8),
                      suffixIcon: Padding(
                        padding: EdgeInsetsDirectional.only(top: 8, bottom: 8),
                        child: SvgPicture.asset('assets/calendar.svg')),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                        borderRadius: BorderRadius.circular(4)
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFF68C93E)),
                        borderRadius: BorderRadius.circular(4)
                      )
                    ),
                    onTap: () async {
                      now = await
                        showDatePicker(
                          context: context, 
                          initialDate: now, 
                          firstDate: now, 
                          lastDate: DateTime(DateTime.now().year + 1),
                          builder: (BuildContext context, Widget child) {
                            return Theme(
                              child: child,
                              data: ThemeData.light().copyWith(
                                primaryColor: Color(0xFF68C93E),
                                accentColor: Color(0xFF68C93E)
                              ),
                            );
                          },
                        );
                        epn.setDate(widget.expiredProduct.product, item.id, now);
                      _dateController.text = Util.format(now);
                      FocusScope.of(context).requestFocus(new FocusNode());
                    }
                  ),
                ),
                SizedBox(width: 16),
                Container(
                  width: 70,
                  height: 44,
                  child: TextFormField(
                    cursorColor: Color(0xFF68C93E),
                    keyboardType: TextInputType.number,
                    style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F)), textAlign: TextAlign.center,
                    controller: _quantityController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                        borderRadius: BorderRadius.circular(4)
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFF68C93E)),
                        borderRadius: BorderRadius.circular(4)
                      ),
                    ),
                    onChanged: (value) {
                      epn.setQuantity(widget.expiredProduct.product, item.id, int.parse(value));
                      _quantityController..text = value
                        ..selection = TextSelection.collapsed(offset: item.quantity.toString().length);
                    },
                  ),
                ),
              ],
            ), 
            SizedBox(width: 28),
            InkWell(
              onTap: () {
                epn.removeExpired(item);
              },
              child: Icon(Icons.clear, color: Colors.grey)
            ),
          ],
        ),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ExpiredProductNotifier>(
      builder: (_, epn, __) {
        return Container(
          color: Colors.white,
          padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget> [
                  Icon(Icons.check_circle, color: Color(0xFF68C93E), size: 20),
                  SizedBox(width: 16),
                  Container(
                    height: 77.0,
                    width: 77.0,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: widget.expiredProduct.product.thumbnail != null ? NetworkImage(widget.expiredProduct.product.thumbnail) : AssetImage("assets/placeholder_product.png"),
                        fit: BoxFit.fill,
                      ),
                      border: Border.all(color: Color(0xFF68C93E)),
                      borderRadius: BorderRadius.circular(5)
                    ),
                  ),
                  SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.expiredProduct.product.name, 
                          style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold), 
                          textAlign: TextAlign.left,
                          maxLines: 2),
                        SizedBox(height: 16),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                epn.removeProduct(widget.expiredProduct);
                              },
                              child: SvgPicture.asset('assets/trash.svg'))
                        ],
                      )
                    ],
                  ),
                ),
              ],
              ),
              SizedBox(height: 16.0),
              Divider(height: 3, color: Colors.grey),
              SizedBox(height: 16.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      epn.addExpired(widget.expiredProduct);
                    }, 
                    color: Color(0xFFEDECFF),
                    child: Text('+ Tambah Tanggal Kadaluarsa', style: TextStyle(color: Color(0xFF9690DD), fontWeight: FontWeight.bold)),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)
                    ),
                  ),
                  SizedBox(height: 16.0),
                  Row(
                    children: <Widget>[
                      Text('Tanggal Kadaluarsa', style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                      SizedBox(width: 66),
                      Text('Jumlah', style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold))
                    ],
                  ),
                  SizedBox(height: 16.0),
                  ...tanggalKadaluarsaWidgets(epn)
                ],
              )
            ],
          ),
        );
      }
    );
  }
}