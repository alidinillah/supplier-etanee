import 'package:flutter/material.dart';
import 'package:supplier/models/category.dart';
import 'package:supplier/services/category_service.dart';
import 'package:supplier/views/pill_button.dart';

class ProductCategory extends StatelessWidget {
  final Function callback;
  final int selected;
  ProductCategory(this.selected, this.callback);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Category>>(
      future: CategoryService().get(),
      builder: (_, snapshot) {
        if (!snapshot.hasData) return Container();
        return Container(
          height: 35,
          child: ListView.builder(
            padding: EdgeInsets.only(left: 16),
            itemCount: snapshot.hasData ? snapshot.data.length : 0,
            itemBuilder: (_, i) {
              Category m = snapshot.data[i];
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 3),
                child: PillButton(
                  m.name,
                  () => callback(m.id),
                  selected: m.id == selected,
                ),
              );
            },
            scrollDirection: Axis.horizontal,
          ),
        );
      },
    );
  }
}