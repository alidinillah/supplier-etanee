import 'package:flutter/material.dart';
import 'package:supplier/models/product.dart';
import 'package:supplier/views/product/product_detail.dart';
import 'package:supplier/views/product/product_item_button.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  ProductItem(this.product);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => ProductDetail(product)));
      },
      child: Card(
        elevation: 2.0,
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Hero(
              tag: product.id,
              child: ClipRRect(
                child: product.thumbnail != null ? Image.network(product.thumbnail) : Image.asset("assets/placeholder_product.png"),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(4),
                  topLeft: Radius.circular(4)
                ),
              )
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    product.name, 
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Color(0xFF4F4F4F)), 
                    textAlign: TextAlign.start,
                    maxLines: 2
                  ),
                  SizedBox(height: 16),
                  ProductItemButton(product)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}