import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/models/product.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';

class ProductItemButton extends StatelessWidget {
  final Product product;
  ProductItemButton(this.product);

  @override
  Widget build(BuildContext context) {
    return Consumer<ExpiredProductNotifier>(
      builder: (_, epn, __) {
        if(epn.isExist(product))
          return ButtonTheme(
            minWidth: 140,
            height: 33,
            child: RaisedButton(
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
              ),
            onPressed: (){
              epn.addProduct(product);
            }, 
            color: Color(0xFF68C93E),
            child: Text('Pilih', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))
            ),
          );
        return ButtonTheme(
          minWidth: 140,
          height: 33,
          child: RaisedButton(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
            ),
            onPressed: () {}, 
            color: Colors.grey[300],
            child: Text('Terpilih', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))
          ),
        );
      }
    );
  }
}