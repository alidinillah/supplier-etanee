import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/models/product.dart';
import 'package:supplier/notifiers/product_notifier.dart';
import 'package:supplier/notifiers/search_notifier.dart';
import 'package:supplier/services/product_service.dart';
import 'package:supplier/views/checkout/checkout_box.dart';
import 'package:supplier/views/empty/empty_product.dart';
import 'package:supplier/views/loading.dart';
import 'package:supplier/views/product/product_item.dart';

class ProductPage extends StatelessWidget {
  final String search;
  ProductPage(this.search);

  @override
  Widget build(BuildContext context) {
    ProductService productService = Provider.of<ProductService>(context);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ProductNotifier>(
          create: (_) => ProductNotifier()
        ),
        ChangeNotifierProvider<SearchNotifier>(
          create: (_) => SearchNotifier(search)
        ),
      ],
    child: Consumer<ProductNotifier>(
      builder: (_, pn, __) {
        return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1.0,
        title: Consumer<SearchNotifier>(
          builder: (_, sn, __) =>
            Container(
              height: 45,
              child: TextFormField(
                initialValue: sn.search,
                onChanged: (s) => sn.search = s,
                cursorColor: Color(0xFF68C93E),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(6),
                    borderSide: BorderSide(color: Colors.white)
                  ),
                  filled: true,
                  focusColor: Colors.grey,
                  fillColor: Color(0xFFF2F2F2),
                  hintText: "Cari nama produk disini",
                  hintStyle: TextStyle(fontSize: 14),
                  suffixIcon: Icon(Icons.search, color: Colors.grey),
                  contentPadding: EdgeInsets.all(16.0),
                ),
              ),
            ),
          ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 16.0, right: 16.0),
            child: CheckoutBox(),
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget> [
              SizedBox(height: 10),
              Expanded(
                child: FutureBuilder<List<Product>>(
                  future: productService.products(),
                  builder: (context, asy) {
                    if (asy.connectionState == ConnectionState.waiting)
                      return Loading();
                    if (!asy.hasData) return Container();
                    if (asy.hasData && asy.data.isEmpty)
                      return EmptyProduct();
                    return Consumer<SearchNotifier>(
                      builder: (_, sn, __) {
                        List<Product> products = asy.data.where((p) {
                          bool s = p.description
                                  .toLowerCase()
                                  .contains(sn.search.toLowerCase()) ||
                              p.name
                                  .toLowerCase()
                                  .contains(sn.search.toLowerCase());
                          return s;
                        }).toList();
                        if (products.isEmpty) return EmptyProduct();
                        return GridView.builder(
                          itemCount: products.length,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 0.55,
                          ),
                          itemBuilder: (_, i) => ProductItem(products[i]),
                        );
                    }
                    );
                  },
                ),
              ),   
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}