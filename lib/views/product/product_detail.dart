import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/models/product.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/views/checkout/checkout_box.dart';
import 'package:supplier/views/checkout/checkout_product.dart';

class ProductDetail extends StatelessWidget {
  final Product product;
  ProductDetail(this.product);

  @override
  Widget build(BuildContext context) {
    // ExpiredProductNotifier epn = Provider.of<ExpiredProductNotifier>(context);
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Colors.transparent,
      //   elevation: 0,
      //   actions: <Widget>[
      //     Padding(
      //       padding: const EdgeInsets.only(top: 16.0, right: 16.0),
      //       child: CheckoutBox(),
      //     )
      //   ],
      // ),
      extendBodyBehindAppBar: true,
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Center(
                child: Hero(
                  tag: product.id,
                  child: product.imageSize(MediaQuery.of(context).size.width),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: (){
                      Navigator.pop(context);
                    }
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: CheckoutBox(),
                  ),
                ],
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(product.name, style: TextStyle(fontSize: 16, color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                SizedBox(height: 16),
                Text('Deskripsi Produk', style: TextStyle(fontSize: 14,  color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                SizedBox(height: 8),
                Text(product.description,
                  style: TextStyle(color: Colors.grey), textAlign: TextAlign.justify,),
                SizedBox(height: 16),
                Divider(),
                SizedBox(height: 16),
                Text('Informasi Produk', style: TextStyle(fontSize: 14,  color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Berat per karton', style: TextStyle(color: Color(0xFF4F4F4F))),
                    Text(product.weight.toString(), style: TextStyle(color: Colors.grey))
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Item terjual', style: TextStyle(color: Color(0xFF4F4F4F))),
                    Text('20 Karton', style: TextStyle(color: Colors.grey))
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Kondisi', style: TextStyle(color: Color(0xFF4F4F4F))),
                    Text('Fresh', style: TextStyle(color: Colors.grey))
                  ],
                ),
              ]
            ),
          )
        ],
      ),
      bottomNavigationBar: Consumer<ExpiredProductNotifier> (
        builder: (_, bn, __) {
          if (bn.isExist(product))
          return BottomAppBar(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ButtonTheme(
                minWidth: 328,
                height: 48,
                child: RaisedButton(
                  elevation: 0,
                  color: Color(0xFF68C93E),
                  onPressed: (){
                    bn.addProduct(product);
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) => CheckoutProduct()));
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)
                  ),
                  child: Text('Tambahkan', style: TextStyle(fontSize: 16, color: Colors.white)),
                ),
              ),
            )
          );
        return BottomAppBar(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ButtonTheme(
              minWidth: 328,
              height: 48,
              child: RaisedButton(
                elevation: 0,
                color: Colors.grey[300],
                onPressed: (){},
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)
                ),
                child: Text('Tambahkan', style: TextStyle(fontSize: 16, color: Colors.white)),
              ),
            ),
          )
        );
      }
    )  
    );
  }
}