import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/views/product/product_code_detail.dart';

class ProductCode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<ExpiredProductNotifier>(
        builder: (_, epn, __) {
          if (epn == null) return Container();
          return ListView.builder(
            shrinkWrap: true,
            itemCount: epn.expiredProducts.length,
            itemBuilder: (ctx, i) => ProductCodeDetail(epn.expiredProducts[i]),
            physics: ScrollPhysics(),
          );
        }
      ),
    );
  }
}