import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/util.dart';
import 'package:supplier/views/confirm/confirm_product_detail_page.dart';

class ProductCodeDetail extends StatelessWidget {

  final ExpiredProduct expiredProduct;
  ProductCodeDetail(this.expiredProduct);
  
  @override
  Widget build(BuildContext context) {
    return Consumer<ExpiredProductNotifier>(
      builder: (_, epn, __) {
        return Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 16.0),
            child: Card(
              elevation: 0,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[   
                    Text(expiredProduct.product.name, style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                    SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Jumlah Karton', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                        SizedBox(height: 8),
                        Container(
                          height: 40,
                          width: 40,
                          color: Colors.white,
                          child: Center(
                            child: Text(epn.totalQuantity(expiredProduct.product).toString(), 
                            style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F)))
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 8),
                    Divider(),
                    SizedBox(height: 8),
                    ListView.separated(
                      separatorBuilder: (_, __) => Padding(
                        padding: EdgeInsets.only(bottom: 8.0)
                      ), 
                      shrinkWrap: true,
                      itemCount: expiredProduct.expiredQuantities.length,
                      physics: ScrollPhysics(),
                      itemBuilder: (ctx, i) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text('Tanggal Kadaluarsa', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                                  SizedBox(height: 8),
                                  Text(Util.format(expiredProduct.expiredQuantities[i].expired), style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F)),)
                                ],
                              ),
                              Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                  color: Color(0xFFEFFFE9),
                                  borderRadius: BorderRadius.circular(4)
                                ),
                                child: Center(child: Text(expiredProduct.expiredQuantities[i].quantity.toString(), 
                                style: TextStyle(fontSize: 14, color: Color(0xFF68C93E), fontWeight: FontWeight.bold))),
                              ),
                            ],
                          );
                      }
                    ),
                    Center(
                      child: RaisedButton(
                        elevation: 0,
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(
                            builder: (_) => ConfirmProductDetailPage(expiredProduct)));
                        },
                        color: Color(0xFFEDECFF),
                        child: Text('Lihat Kode', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF9690DD))),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          );
      });
      }

    // void getExpiredProducts(context) async {
    //   ExpiredProductService expiredProductService = ExpiredProductService();
    //   expiredProductService
    //     .getExpiredProducts();
    // }
  }