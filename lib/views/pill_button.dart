import 'package:flutter/material.dart';

class PillButton extends StatelessWidget {
  final bool selected;
  final Color color;
  final String text;
  final VoidCallback onTap;
  PillButton(
    this.text,
    this.onTap, {
    this.selected = false,
    this.color = Colors.green,
  });
  @override
  Widget build(BuildContext context) {
    return selected
        ? RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
                side: BorderSide(color: color)),
            onPressed: onTap,
            color: color,
            textColor: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Text(text, style: TextStyle(fontWeight: FontWeight.bold)),
            ),
          )
        : FlatButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
                side: BorderSide(color: color)),
            color: Colors.white,
            textColor: color,
            padding: EdgeInsets.all(5.0),
            onPressed: onTap,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Text(
                text,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          );
  }
}