import 'package:flutter/material.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/services/expired_product_service.dart';
import 'package:supplier/views/loading.dart';
import 'package:supplier/views/print/print_page.dart';

class CodeUpdate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<GetExpiredProduct>>(
      future: ExpiredProductService().getExpiredProducts(),
      builder: (_, snapshot) {
        if(snapshot.data == null) {
          return Loading();
        } else {
          return ListView.builder(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data.length - 48,
            itemBuilder: (_, i) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      elevation: 0,
                      child: Padding(
                        padding: EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(snapshot.data[i].product.name, style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                            SizedBox(height: 8),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text('Jumlah : ', style: TextStyle(color: Color(0xFF4F4F4F))),
                                Text((i+1).toString() + ' karton', style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold))
                              ],
                            ),
                            SizedBox(height: 8),
                            Center(
                              child: RaisedButton(
                                elevation: 0,
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => PrintPage()));
                                },
                                color: Color(0xFFEDECFF),
                                child: Text('Cetak Ulang', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF9690DD))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
          );
        }
      }
    );
  }
}