import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:supplier/models/account.dart';
import 'package:supplier/notifiers/auth_notifier.dart';
import 'package:supplier/views/checkout/checkout_box.dart';
import 'package:supplier/views/code_check.dart';
import 'package:supplier/views/home/code_update.dart';
import 'package:supplier/views/loading.dart';
import 'package:supplier/views/menu/bottom_menu.dart';
import 'package:supplier/views/product/product_page.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthNotifier notifier = Provider.of<AuthNotifier>(context, listen: false);
    Account account = notifier.account;
    double width = MediaQuery.of(context).size.width;

    Widget background = SvgPicture.asset('assets/circle_background.svg',
      fit: BoxFit.fill,
      width: width
    );
    if (account == null) return Loading();
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      appBar: AppBar(
        elevation: 0.0,
        title: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SvgPicture.asset('assets/supplier.svg', height: 36),
              CheckoutBox()
            ],
          ),
        ),
        backgroundColor: Colors.white,
      ),
      extendBodyBehindAppBar: true,
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 315,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20.0)),
                  color: Colors.white,
                ),
              ),
              background,
              Container(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text('Halo, ${account.firstName}', style: TextStyle(fontSize: 20, color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold))
                        ]
                      ),
                      SizedBox(height: 8),
                      Text('Selamat datang', style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F))),
                      SizedBox(height: 16.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) => ProductPage(null)));
                            },
                            child: Container(
                              height: 202,
                              width: 156,
                              padding: EdgeInsets.all(16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                gradient: LinearGradient(
                                  begin: Alignment.topRight, 
                                  end: Alignment.topLeft,
                                  colors: [Color(0xFF68C93E), Color(0xFF78E87C)])
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget> [
                                  SvgPicture.asset('assets/create.svg'),
                                  SizedBox(height: 34),
                                  Text('Buat QR Code', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white)),
                                  SizedBox(height: 8),
                                  Text('Beragam pilihan produk', style: TextStyle(fontSize: 12, color: Colors.white)),
                                ]
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) => CodeCheck(null)));
                            },
                            child: Container(
                              height: 202,
                              width: 156,
                              padding: EdgeInsets.all(16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                gradient: LinearGradient(
                                  begin: Alignment.topRight, 
                                  end: Alignment.topLeft,
                                  colors: [Color(0xFF615C9B), Color(0xFFAFA8FF)])
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget> [
                                  SvgPicture.asset('assets/search.svg'),
                                  SizedBox(height: 34),
                                  Text('Cek QR Code', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white)),
                                  SizedBox(height: 8),
                                  Text('Pelacakan kode produk', style: TextStyle(fontSize: 12, color: Colors.white)),
                                ]
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Kode Terbaru', style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                InkWell(
                  onTap: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (context) => BottomMenu(1)));
                    },
                  child: Text('Lihat Semua', style: TextStyle(color: Color(0xFF68C93E)))),
              ],
            ),
          ),
          SizedBox(height: 8),
          CodeUpdate(),
        ],
      ),
    );
  }
}