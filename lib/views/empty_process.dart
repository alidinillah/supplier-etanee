import 'package:flutter/material.dart';
import 'package:supplier/views/menu/bottom_menu.dart';

class EmptyProcess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/empty_stock.png",
              width: 300,
            ),
            SizedBox(height: 32),
            Text('Tidak ada proses pembuatan QR Code produk', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 14)),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            minWidth: 328,
            height: 48,
            child: RaisedButton(
              elevation: 0,
              color: Color(0xFF68C93E),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => BottomMenu(0)),
                  (Route<dynamic> route) => false,
                );
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
              ),
              child: Text('Ke Halaman Utama', style: TextStyle(fontSize: 16, color: Colors.white)),
            ),
          ),
        )
      ),
    );
  }
}
