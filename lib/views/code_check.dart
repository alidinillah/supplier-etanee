import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/notifiers/search_notifier.dart';
import 'package:supplier/services/expired_product_service.dart';
import 'package:supplier/util.dart';
import 'package:supplier/views/code_check_items.dart';
import 'package:supplier/views/loading.dart';
import 'package:supplier/views/print/print_page.dart';

class CodeCheck extends StatelessWidget {
  final String search;
  CodeCheck(this.search);

  
  // TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SearchNotifier> (
          create: (_) => SearchNotifier(search)
        ),
      ],
      child: Scaffold(
        appBar: AppBar(
          elevation: 1.5,
          title: Text('Cek QR Code'),
        ),
        backgroundColor: Colors.white,
        body: 
        Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
          child: Container(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Text('Masukkan kode disini', style: TextStyle(color: Color(0xFF4F4F4F))),
                SizedBox(height: 8),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 40,
                        child: Consumer<SearchNotifier>(
                          builder: (_, sn, __) => TextField(
                            // controller: _searchController,
                            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            cursorColor: Color(0xFF68C93E),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(8.0),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[300])
                              ),
                              hintText: '',
                              border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(4.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    Container(
                      height: 40,
                      child: FlatButton(
                        onPressed: _buildList, 
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4)
                        ),
                        color: Color(0xFF68C93E),
                        child: Text('Cek Kode', style: TextStyle(color: Colors.white),)
                      ),
                    ),
                  ],
                ),
                FutureBuilder<List<GetExpiredProduct>> (
                  future: ExpiredProductService().getExpiredProducts(),
                  builder: (_, snapshot) {
                    if(snapshot.data == null) {
                      return Loading();
                    } else {
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: snapshot.data.length,
                        itemBuilder: (_, i) {
                          return Card(
                            elevation: 2.0,
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget> [
                                  Container(
                                    height: 77.0,
                                    width: 77.0,
                                    child: QrImage(data: snapshot.data[i].code)
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        SizedBox(height: 7),
                                        Text(snapshot.data[i].code, style: TextStyle(fontSize: 14, color: Color(0xFF68C93E), fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                                        SizedBox(height: 8),
                                        Text('${snapshot.data[i].product.name}', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                                        SizedBox(height: 8),
                                        Row(
                                          children: <Widget>[
                                            Text('Kadaluarsa  :  ', style: TextStyle(fontSize: 12)),
                                            Text(Util.format(snapshot.data[i].expired).toString(), style: TextStyle(fontSize: 12))
                                          ],
                                        ),
                                        SizedBox(height: 16),
                                        RaisedButton(
                                          elevation: 0,
                                          onPressed: () {
                                            Navigator.push(context, MaterialPageRoute(
                                              builder: (context) => PrintPage()));
                                          },
                                          color: Color(0xFFEDECFF),
                                          child: Text('Cetak Ulang', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF9690DD))),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(20)
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }
                      );
                    }
                  }
                )
                // SizedBox(height: 16),
                // Card(
                //   elevation: 2.0,
                //   child: Padding(
                //     padding: const EdgeInsets.all(16.0),
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.start,
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: <Widget> [
                //         Container(
                //           height: 100.0,
                //           width: 100.0,
                //           child: QrImage(data: 'AG6HWB9K')
                //         ),
                //         SizedBox(width: 16),
                //         Expanded(
                //           child: Column(
                //             crossAxisAlignment: CrossAxisAlignment.start,
                //             children: <Widget>[
                //               SizedBox(height: 5),
                //               Text('AG6HWB9K', style: TextStyle(fontSize: 18, color: Color(0xFF68C93E), fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                //               SizedBox(height: 16),
                //               Text('Nama  : Ayam'),
                //               SizedBox(height: 16),
                //               Text('Kadaluarsa  : 06 April 2020'),
                //               SizedBox(height: 16),
                //               RaisedButton(
                //                 elevation: 0,
                //                 onPressed: () {},
                //                 color: Color(0xFFEDECFF),
                //                 child: Text('Cetak Ulang', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF9690DD))),
                //                 shape: RoundedRectangleBorder(
                //                   borderRadius: BorderRadius.circular(20)
                //                 ),
                //               ),
                //             ],
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                // SizedBox(height: 16),
                // Card(
                //   elevation: 2.0,
                //   child: Padding(
                //     padding: const EdgeInsets.all(16.0),
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.start,
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: <Widget> [
                //         Container(
                //           height: 100.0,
                //           width: 100.0,
                //           child: QrImage(data: 'DY7HWB3K')
                //         ),
                //         SizedBox(width: 16),
                //         Expanded(
                //           child: Column(
                //             crossAxisAlignment: CrossAxisAlignment.start,
                //             children: <Widget>[
                //               SizedBox(height: 5),
                //               Text('DY7HWB3K', style: TextStyle(fontSize: 18, color: Color(0xFF68C93E), fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                //               SizedBox(height: 16),
                //               Text('Nama  : Daging Sapi'),
                //               SizedBox(height: 16),
                //               Text('Kadaluarsa  : 20 Juni 2020'),
                //               SizedBox(height: 16),
                //               RaisedButton(
                //                 elevation: 0,
                //                 onPressed: () {},
                //                 color: Color(0xFFEDECFF),
                //                 child: Text('Cetak Ulang', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF9690DD))),
                //                 shape: RoundedRectangleBorder(
                //                   borderRadius: BorderRadius.circular(20)
                //                 ),
                //               ),
                //             ],
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  
  Widget _buildList() {
    return FutureBuilder<List<GetExpiredProduct>>(
      future: ExpiredProductService().getExpiredProducts(),
      builder: (_, snapshot) {
        if (snapshot.data == null) {
          return Loading();
        } else {
          return Consumer<SearchNotifier> (
            builder: (_, sn, __) {
              List<GetExpiredProduct> expProducts = snapshot.data.where((e) {
                bool s = e.code
                          .toString()
                          .toLowerCase()
                          .contains(sn.search.toLowerCase()) ||
                        e.product.name
                          .toLowerCase()
                          .contains(sn.search.toLowerCase());
                        return s;
              }).toList();
              if(expProducts.isEmpty) return Text('Tidak ditemukan');
              return ListView.builder(
                itemCount: expProducts == null ? 0 : expProducts.length,
                itemBuilder: (_, i) {
                  return CodeCheckItems(expProducts[i]);
                }
              );
            }
          );
        }
      }
    );
  }
}