import 'package:flutter/material.dart';
import 'package:supplier/util.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.5,
        title: Text('Lupa Kata Sandi'),
      ),
      key: _scaffoldKey,
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(36.0, 49.0, 36.0, 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Email', style: TextStyle(color: Color(0xFF4F4F4F))),
                SizedBox(height: 4),
                Container(
                  height: 50,
                  child: TextFormField(
                    controller: _emailController,
                    cursorColor: Color(0xFF68C93E),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFF68C93E))
                      ),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(4), borderSide: BorderSide(color: Color(0xFF68C93E)))
                    ),
                  ),
                ),
                SizedBox(height: 24),
                ButtonTheme(
                  minWidth: 328.0,
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: _forgotPassword,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                    color: Color(0xFF68C93E),
                    elevation: 0,
                    child: Text(
                      'Konfirmasi email',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  bool _validate() {
    if (_emailController.text.isEmpty){
      _scaffoldKey.currentState
        .showSnackBar(Util.getSnackBarError("Masukkan email Anda"));
      return false;
    }
    return true;
  }

  void showError(message) {
    _scaffoldKey.currentState.showSnackBar(Util.getSnackBarError(message));
  }

  
  void _forgotPassword() async{
    if(!_validate()) return;
    // final svc = Provider.of<AccountService>(context);
    // final notifier = Provider.of<AuthNotifier>(context);
    // Account account =
    // await svc
    //   .login(_emailController.text)
    //   .catchError((e) => showError(e.response.data["message"]));

    // if (account != null){
    //   notifier.account = account;
    //   notifier.status = AuthStatus.Authenticated;
    // }
  }
}