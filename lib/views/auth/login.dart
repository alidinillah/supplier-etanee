import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:supplier/models/account.dart';
import 'package:supplier/notifiers/auth_notifier.dart';
import 'package:supplier/services/account_service.dart';
import 'package:supplier/util.dart';
import 'package:supplier/views/auth/forgot_password.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _hidePassword = true;
  bool btnEnable = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  void togglePasswordVisibility(){
    setState(() {
      _hidePassword = !_hidePassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(91.0, 62.0, 92.0, 0.0),
            child: Center(
              child: SvgPicture.asset('assets/supplier.svg'),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(36.0, 49.0, 36.0, 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Email', style: TextStyle(color: Color(0xFF4F4F4F))),
                SizedBox(height: 4),
                Container(
                  height: 50,
                  child: TextFormField(
                    controller: _emailController,
                    cursorColor: Color(0xFF68C93E),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFF68C93E))
                      ),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(4), borderSide: BorderSide(color: Color(0xFF68C93E)))
                    ),
                  ),
                ),
                SizedBox(height: 16),
                Text('Kata Sandi', style: TextStyle(color: Color(0xFF4F4F4F))),
                SizedBox(height: 4),
                Container(
                  height: 50,
                  child: TextFormField(
                    controller: _passwordController,
                    cursorColor: Color(0xFF68C93E),
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: _hidePassword,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFF68C93E))
                      ),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(4)),
                    suffixIcon: InkWell(
                      onTap: (){
                        togglePasswordVisibility();
                      },
                      child: Icon(
                        _hidePassword ? Icons.visibility_off : Icons.visibility,
                        color: _hidePassword ? Colors.grey : Color(0xFF68C93E),
                      ),
                    )
                    ),
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) => ForgotPassword()));
                      },
                      child: Text('Lupa kata sandi?', style: TextStyle(color: Color(0xFF68C93E)), textAlign: TextAlign.right)),
                  ],
                ),
                SizedBox(height: 24),
                ButtonTheme(
                  minWidth: 328.0,
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: _login,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                    color: Color(0xFF68C93E),
                    elevation: 0,
                    child: Text(
                      'Masuk',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  bool _validate() {
    if (_emailController.text.isEmpty){
      _scaffoldKey.currentState
        .showSnackBar(Util.getSnackBarError("Masukkan email Anda"));
      return false;
    }
    if (_passwordController.text.isEmpty){
      _scaffoldKey.currentState
        .showSnackBar(Util.getSnackBarError("Masukkan kata sandi Anda"));
      return false;
    }
    return true;
  }

  void showError(message) {
    _scaffoldKey.currentState.showSnackBar(Util.getSnackBarError(message));
  }

  
  void _login() async{
    if(!_validate()) return;
    final svc = Provider.of<AccountService>(context);
    final notifier = Provider.of<AuthNotifier>(context);
    Account account =
    await svc
      .login(_emailController.text, _passwordController.text)
      .catchError((e) => showError(e.response.data["message"]));

    if (account != null){
      notifier.account = account;
      notifier.status = AuthStatus.Authenticated;
    }
  }
}