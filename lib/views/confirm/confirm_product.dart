import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:supplier/views/bluetooth/bluetooth_off.dart';
import 'package:supplier/views/confirm/confirm_product_page.dart';

class ConfirmProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xFFF2F2F2),
      appBar: AppBar(
        elevation: 1.5,
        title: Text('Konfirmasi Produk')),
        body: ConfirmProductPage(),
        bottomNavigationBar: BottomAppBar(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ButtonTheme(
                minWidth: 328,
                height: 48,
                child: RaisedButton(
                  elevation: 0,
                  color: Color(0xFF68C93E),
                  onPressed: () {
                    showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      backgroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)
                      ),
                      builder: (context){
                        return FractionallySizedBox(
                          heightFactor: 0.6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 150.0, top: 8.0),
                                child: SvgPicture.asset('assets/hide.svg'),
                              ),
                              ListTile(
                                title: Text('Cara Mencetak Kode Produk', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0XFF4F4F4F))),
                              ),
                              Divider(),
                              Container(
                                padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('1. Aktifkan Bluetooth di perangkat Anda', style: TextStyle(color: Color(0XFF4F4F4F))),
                                    SizedBox(height: 16),
                                    Text('2. Nyalakan Printer Anda', style: TextStyle(color: Color(0XFF4F4F4F))),
                                    SizedBox(height: 16),
                                    Text('3. Pastikan Printer memiliki koneksi Bluetooth', style: TextStyle(color: Color(0XFF4F4F4F))),
                                    SizedBox(height: 16),
                                    Text('4. Aktifkan Bluetooth pada Printer', style: TextStyle(color: Color(0XFF4F4F4F))),
                                    SizedBox(height: 16),
                                    Text('5. Sambungkan perangkat Anda dengan Printer melalui koneksi bluetooth', style: TextStyle(color: Color(0XFF4F4F4F))),
                                    SizedBox(height: 8),
                                    Text('6. Cetak QR Code produk', style: TextStyle(color: Color(0XFF4F4F4F))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0, top: 24.0, right: 8.0),
                                child: ButtonTheme(
                                  minWidth: 370,
                                  height: 48,
                                  child: RaisedButton(
                                    elevation: 0,
                                    onPressed: (){
                                      Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => BluetoothOff()));
                                    },
                                    color: Color(0xFF68C93E),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(4)
                                    ),
                                    child: Text('Lanjutkan', style: TextStyle(fontSize: 16, color: Colors.white)),
                                  ),
                                ),
                              ),
                            ],
                        ));
                      }
                    );
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)
                  ),
                  child: Text('Cetak Kode', style: TextStyle(fontSize: 16, color: Colors.white)),
                ),
              ),
            ),
          ),
        );
  }
}