import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'dart:core';

import 'package:supplier/util.dart';

class ConfirmProductDetailPage extends StatelessWidget {
  final ExpiredProduct expiredProduct;
  ConfirmProductDetailPage(this.expiredProduct);
  
  @override
  Widget build(BuildContext context) {
    return Consumer<ExpiredProductNotifier>(
      builder: (_, epn, __) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: Text('Detail Kode'),
            elevation: 1.5,
          ),
          backgroundColor: Color(0xFFF2F2F2),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[  
                        Text(expiredProduct.product.name, style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                        SizedBox(height: 16),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Jumlah Karton', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                            Container(
                              height: 40,
                              width: 40,
                              color: Colors.white,
                              child: Text(epn.totalQuantity(expiredProduct.product).toString(), style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 14)))
                          ],
                        ),
                      ],
                  ),
                ),
                SizedBox(height: 16.0),
                ListView.separated(
                  separatorBuilder: (_, __) => Padding(
                padding: EdgeInsets.only(bottom: 16.0)),
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemCount: expiredProduct.expiredQuantities.length,
                  itemBuilder: (_, i) {
                    return Container(
                            padding: EdgeInsets.all(16),
                            color: Colors.white,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Tanggal Kadaluarsa', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                                        SizedBox(height: 16),
                                        Text(Util.format(expiredProduct.expiredQuantities[i].expired), style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F))),
                                      ],
                                    ),
                                    Container(
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                        color: Color(0xFFEDECFF),
                                        borderRadius: BorderRadius.circular(4)
                                      ),
                                      child: Center(child: Text(expiredProduct.expiredQuantities[i].quantity.toString(), style: TextStyle(fontSize: 14, color: Color(0xFF9690DD), fontWeight: FontWeight.bold))),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 16),
                                ExpansionTile(
                                  title: Text('Kode QR', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                                  children: <Widget>[
                                    SizedBox(
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: expiredProduct.expiredQuantities[i].quantity,
                                        itemBuilder: (_, j) {
                                          return _qrCodeItem(8);
                                        }
                                      )
                                    )
                                  ],
                                ),
                              ],
                            ),
                          );
                  }
                )
              ],
            ),
          ),
        );
      }
    );
  }

  Widget _qrCodeItem(int y) {
    const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    String random(int strlen) {
      Random rnd = Random(DateTime.now().millisecondsSinceEpoch);
      var result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }
    return ListTile(
          leading: QrImage(data: random(8), size: 50),
          title: Text(random(8), style: TextStyle(color: Color(0xFF68C93E), fontWeight: FontWeight.bold)),
          dense: true,
        );
  }
}