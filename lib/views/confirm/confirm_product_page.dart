import 'package:flutter/material.dart';
import 'package:supplier/views/product/product_code.dart';

class ConfirmProductPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      resizeToAvoidBottomInset: true,
      body:
        ListView(
          shrinkWrap: true,
          children: <Widget>[
            ProductCode()
          ]
        ),
    );
  }
}