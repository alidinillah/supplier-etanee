import 'package:flutter/material.dart';
import 'package:supplier/views/checkout/checkout_action.dart';
import 'package:supplier/views/checkout/checkout_product_page.dart';

class CheckoutProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xFFF2F2F2),
      appBar: AppBar(
        elevation: 1.5,
        title: Text('Tentukan Tanggal Kadaluarsa')),
        body: CheckoutProductPage(),
        bottomNavigationBar: CheckoutAction(
          // () => submitCheckout(context)
        ),
    );
  }

  // void submitCheckout(BuildContext context){
  //   CheckoutNotifier checkoutNotifier =
  //     Provider.of<CheckoutNotifier>(context, listen: false);
  //   BoxNotifier boxNotifier =
  //     Provider.of<BoxNotifier>(context, listen: false);
  //   if (checkoutNotifier.expiredDates == null) {
  //     Util.showError(
  //       context, 
  //       'Silakan isi tanggal kadaluarsa terlebih dahulu', 
  //       Image.asset('assets/empty_stock.png', width: 140)
  //     );
  //     return;
  //   }
  // }
}