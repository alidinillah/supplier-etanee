import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/services/expired_product_service.dart';
import 'package:supplier/views/confirm/confirm_product.dart';
import 'package:supplier/views/product/product_page.dart';

class CheckoutAction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
      Container(
        padding: EdgeInsets.all(8),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ButtonTheme(
              minWidth: 48,
              height: 48,
              child: RaisedButton(
                elevation: 0,
                color: Color(0xFFEEEEEE),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(
                    builder: (_) => ProductPage(null)));
                },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)
                ),
                child: SvgPicture.asset('assets/box_add.svg')
              ),
            ),
            ButtonTheme(
              minWidth: 275,
              height: 48,
              child: RaisedButton(
                elevation: 0,
                color: Color(0xFF68C93E),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (_) => ConfirmProduct()));
                  createExpiredProducts(context);
                },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)
                ),
                child: Text('Generate QR Code', style: TextStyle(fontSize: 16, color: Colors.white)),
              ),
            ),
          ],
        ),
      );
  }

  void createExpiredProducts(context) async {
    ExpiredProductNotifier expiredProductNotifier = Provider.of<ExpiredProductNotifier>(context, listen: false);
    ExpiredProductService expiredProductService = ExpiredProductService();
    expiredProductService
      .createExpiredProducts(expiredProductNotifier.expiredProducts);
  }    
}