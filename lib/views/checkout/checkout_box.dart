import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/views/checkout/checkout_product.dart';
import 'package:supplier/views/empty_process.dart';

class CheckoutBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ExpiredProductNotifier>(
      builder: (_, epn, __) => InkWell(
        child: Badge(
          position: BadgePosition.topRight(),
          badgeContent: Text(
            "${epn.total}",
            style: TextStyle(color: Colors.white),
          ),
          animationType: BadgeAnimationType.scale,
          child: SvgPicture.asset('assets/box.svg')
        ),
        onTap: () {
          if (epn.expiredProducts.isEmpty) {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (ctx) => EmptyProcess()),
            );
          } else {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (ctx) => CheckoutProduct()),
              );
            }
        },
      )
    );
  }
}