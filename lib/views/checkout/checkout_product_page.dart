import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/views/empty_process.dart';
import 'package:supplier/views/product/product_checkout.dart';

class CheckoutProductPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F2F2),
      resizeToAvoidBottomInset: true,
      body:
        ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16),
              color: Colors.white,
              height: 56,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(Icons.check_circle, color: Color(0xFF68C93E), size: 20),
                      SizedBox(width: 8),
                      Text('Pilih semua produk', style: TextStyle(color: Color(0xFF4F4F4F))),
                    ],
                  ),
                  Consumer<ExpiredProductNotifier>(
                    builder: (_, bn, __) {
                      return 
                        InkWell(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0)
                                  ),
                                  child: Container(
                                    height: 231,
                                    width: 328,
                                    child: Padding(
                                      padding: EdgeInsets.all(16.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          SizedBox(height: 10.0),
                                          Column(
                                            children: <Widget>[
                                              Text('Hapus Semua Produk', style: TextStyle(fontSize: 16, color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                                              SizedBox(height: 16.0),
                                              Text('Apakah Anda yakin ingin menghapus semua produk?', style: TextStyle(fontSize: 12, color: Color(0xFF4F4F4F)), textAlign: TextAlign.center,),
                                            ],
                                          ),
                                          SizedBox(height: 32.0),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              FlatButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(4.0)                      
                                                ),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                }, 
                                                child: Text(
                                                  'Batalkan',
                                                  style: TextStyle(color: Colors.grey, fontSize: 14),
                                                ),
                                              ),
                                              FlatButton(
                                                color: Colors.red,
                                                onPressed: () {
                                                  bn.cleanProduct();
                                                  Navigator.push(context, MaterialPageRoute(
                                                    builder: (_) => EmptyProcess()));
                                                }, 
                                                child: Text(
                                                  'Hapus',
                                                  style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                              );
                              }
                            );
                          },
                          child: Text('Hapus', style: TextStyle(color: Colors.red)),
                        );
                    })
                  ],
                ),
              ),
              SizedBox(height: 8),
              ProductCheckout()
            ],
          )
        );
  }
}