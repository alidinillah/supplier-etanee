import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:supplier/views/print/print.dart';

class BluetoothOn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            SizedBox(height: 5),
            Column(
              children: <Widget>[
                SvgPicture.asset('assets/check_circle.svg'),
                SizedBox(height: 48),
                Text('Bluetooth Aktif', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Color(0xFF4F4F4F))),
                SizedBox(height: 8),
                Text('Sekarang Anda dapat mencetak QR Code sesuai pengemasan yang telah dilakukan.', style: TextStyle(height: 1.5, color: Color(0XFF4F4F4F)), textAlign: TextAlign.center),
              ]
            ),
            Column(
              children: <Widget>[
                ButtonTheme(
                  minWidth: 328,
                  height: 48,
                  child: RaisedButton(
                    elevation: 0,
                    color: Color(0xFF68C93E),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                      builder: (context) => Print()));
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4)
                    ),
                    child: Text('Cetak QR Code', style: TextStyle(fontSize: 16, color: Colors.white)),
                  ),
                ),
                SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: InkWell(
                    onTap: () => _onCancel(context),
                    child: Text('Batalkan', style: TextStyle(fontSize: 16, color: Colors.red, fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            )
          ]
        ),
      ),
    );
  }

  _onCancel(BuildContext context) {
    showDialog(context: context,
    builder: (BuildContext context) {
      return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0)
      ),
      child: Container(
        height: 231,
        width: 328,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(height: 10.0),
              Column(
                children: <Widget>[
                  Text('Batalkan Pencetakan', style: TextStyle(fontSize: 16, color: Color(0XFF4F4F4F), fontWeight: FontWeight.bold)),
                  SizedBox(height: 16.0),
                  Text('Apakah Anda yakin ingin membatalkan pencetakan QR Code produk?', style: TextStyle(fontSize: 12, color: Color(0XFF4F4F4F)), textAlign: TextAlign.center),
                ],
              ),
              SizedBox(height: 32.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  FlatButton(
                    color: Colors.white,
                    onPressed: () {}, 
                    child: Text(
                      "Ya, Batalkan",
                      style: TextStyle(color: Colors.red, fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                  ),
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0)                      
                    ),
                    color: Color(0xFF68C93E),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                      builder: (context) => Print()));
                    }, 
                    child: Text(
                      "Lanjutkan",
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
    }
  );
  }
}