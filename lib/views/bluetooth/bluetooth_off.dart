import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:supplier/views/print/print_page.dart';

class BluetoothOff extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            SvgPicture.asset('assets/bluetooth.svg'),
            SizedBox(height: 48),
            Text('Sambungkan Bluetooth', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold)),
            SizedBox(height: 8),
            Text('Untuk melanjutkan proses cetak QR Code produk, Anda perlu koneksi Bluetooth dan menghubungkan dengan Printer Bluetooth Anda.', style: TextStyle(color: Color(0XFF4F4F4F), height: 1.5), textAlign: TextAlign.center)
          ]
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            minWidth: 328,
            height: 48,
            child: RaisedButton(
              elevation: 0,
              color: Color(0xFF68C93E),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => PrintPage()));
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
              ),
              child: Text('Sambungkan', style: TextStyle(fontSize: 16, color: Colors.white)),
            ),
          ),
        )
      ),
    );
  }
}