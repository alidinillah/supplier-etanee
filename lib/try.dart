import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/services/expired_product_service.dart';
import 'package:supplier/util.dart';
import 'package:supplier/views/loading.dart';

class Try extends StatelessWidget {
  final ExpiredProduct expiredProduct;
  Try(this.expiredProduct);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.5,
        title: Text('Detail Kode'),
      ),
      backgroundColor: Color(0xFFF2F2F2),
      body: Column(
        children: <Widget>[
          FutureBuilder<List<GetExpiredProduct>>(
            future: ExpiredProductService().getExpiredProducts(),
            builder: (_, snapshot) {
              if(snapshot.data == null) {
                return Loading();
              } else {
              return ListView.builder(
                shrinkWrap: true,
                itemCount: snapshot.data.length,
                itemBuilder: (_, i) {
                  if(snapshot.data[i].product.name == expiredProduct.product.name) {
                    return Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(16),
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(snapshot.data[i].product.name, style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                              SizedBox(height: 16),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget> [
                                  Text('Jumlah Karton', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                                  Container(
                                    height: 40,
                                    width: 40,
                                    color: Colors.white,
                                    child: Center(child: Text((i+1).toString(), style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold))),
                                  ),
                                ]
                              )
                            ],
                          )
                        ),
                        SizedBox(height: 16),
                        Container(
                          padding: EdgeInsets.all(16),
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text('Tanggal Kadaluarsa', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                                      SizedBox(height: 16),
                                      Text(Util.format(snapshot.data[i].expired), style: TextStyle(fontSize: 14, color: Color(0xFF4F4F4F))),
                                    ],
                                  ),
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      color: Color(0xFFEDECFF),
                                      borderRadius: BorderRadius.circular(4)
                                    ),
                                    child: Center(child: Text((i+1).toString(), style: TextStyle(fontSize: 14, color: Color(0xFF9690DD), fontWeight: FontWeight.bold))),
                                  ),
                                ],
                              ),
                              SizedBox(height: 16),
                              ExpansionTile(
                                title: Text('Kode QR', style: TextStyle(fontSize: 12, color: Color(0xFF828282))),
                                children: <Widget>[
                                  SizedBox(
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      physics: ScrollPhysics(),
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (_, j) {
                                        if(snapshot.data[j].expired == snapshot.data[i].expired) {
                                          return _qrCodeItem(snapshot.data[i].code);
                                        } else {
                                          return null;
                                        }
                                      }
                                    )
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return null;
            //         Text('Produk Tidak Ditemukan', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 14, fontWeight: FontWeight.bold)),
            // SizedBox(height: 16),
            // Text('Masukkan kembali nama produk', style: TextStyle(color: Color(0XFF4F4F4F), fontSize: 14)),
                  }
                });
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _qrCodeItem(String code) {
    return ListTile(
      dense: true,
      title: Text(code, style: TextStyle(fontSize: 14, color: Color(0xFF68C93E), fontWeight: FontWeight.bold)),
      leading: QrImage(
        data: code,
        size: 50),
      onLongPress: () {},
    );
  }
}
