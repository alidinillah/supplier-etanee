import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supplier/etanee_color.dart';
import 'package:supplier/models/account.dart';
import 'package:supplier/models/product.dart';
import 'package:supplier/notifiers/auth_notifier.dart';
import 'package:supplier/notifiers/expired_product_notifier.dart';
import 'package:supplier/services/account_service.dart';
import 'package:supplier/services/product_service.dart';
import 'package:supplier/views/auth/login.dart';
import 'package:supplier/views/loading.dart';
import 'package:supplier/views/menu/bottom_menu.dart';

void main() => runApp(MyApp()
  );

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
    providers: [
      FutureProvider<List<Product>>.value(
        value: ProductService().products().catchError((e) => print(e)),
      ),
      Provider<AccountService>(
        create: (_) => AccountService(),
      ),
      Provider<ProductService>(
        create: (_) => ProductService(),
      ),
      ChangeNotifierProvider<AuthNotifier>(
        create: (_) => AuthNotifier()
      ),
      Provider<Account>.value(
        value: Account(),
      ),
      ChangeNotifierProvider<ExpiredProductNotifier>(
        create: (_) => ExpiredProductNotifier())
  ],
  child: MaterialApp(
    debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: "poppins",
        primaryTextTheme: TextTheme(
          title: TextStyle(color: Color(0xFF4F4F4F))
        ),
        primarySwatch: white
      ),
      home: 
      Consumer<AuthNotifier>(
        builder: (_, authNotifier, __) {
          switch (authNotifier.status) {
            case AuthStatus.Unauthenticated:
              return Login();
            case AuthStatus.Authenticated:
              return BottomMenu(0);
            default:
              return Loading();
          }
        },
      ),
    ),
  );
  }
}

MaterialColor white = MaterialColor(
  0xFFFFFFFF,
  
  <int, Color>{
    50: Color(EtaneeColor.main),
    100: Color(EtaneeColor.main),
    200: Color(EtaneeColor.main),
    300: Color(EtaneeColor.main),
    400: Color(EtaneeColor.main),
    500: Color(EtaneeColor.main),
    600: Color(EtaneeColor.main),
    700: Color(EtaneeColor.main),
    800: Color(EtaneeColor.main),
    900: Color(EtaneeColor.main),
  },
);
